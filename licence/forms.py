from django import forms

from .models import Utilisateur, Editeur, Logiciel, Tarif, Demande, Serveur, Metrique, Dsi

class UserForm(forms.ModelForm):

    class Meta:
        model = Utilisateur
        fields = ('cp', 'mail', 'nom', 'prenom', "entite", "admin")


class EditeurForm(forms.ModelForm):

    class Meta:
        model = Editeur
        fields = '__all__'

class LogicielForm(forms.ModelForm):
    class Meta:
        model = Logiciel
        fields = ('__all__')

class TarifForm(forms.ModelForm):
    class Meta:
        model = Tarif
        fields = '__all__'


class DemandeForm(forms.ModelForm):
    class Meta:
        model = Demande
        fields = '__all__'

class ServeurForm(forms.ModelForm):
    class Meta:
        model = Serveur
        fields = '__all__'

class DsiForm(forms.ModelForm):

    class Meta:
        model = Dsi
        fields = '__all__'

class MetriqueForm(forms.ModelForm):

    class Meta:
        model = Metrique
        fields = '__all__'