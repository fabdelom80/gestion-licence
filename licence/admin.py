from django.contrib import admin

from .models import Serveur, Editeur, Logiciel, Tarif, Utilisateur, Demande


admin.site.register(Serveur)
