from django.urls import path

from . import views

app_name = 'licence' #espace de travail pour differente application

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('utilisateur/add', views.new_utilisateur, name='new_utilisateur'),
    path('dsi/add', views.new_dsi, name='new_dsi'),
    path('metrique/add', views.new_metrique, name='new_metrique'),
    path('editeur/add', views.new_editeur, name='new_editeur'),
    path('logiciel/add', views.new_logiciel, name='new_logiciel'),
    path('tarif/add', views.new_tarif, name='new_tarif'),
    path('demande/add', views.new_demande, name='new_demande'),
    path('serveur/add', views.new_serveur, name='new_serveur'),
    path('logiciel', views.allLogiciel, name='getAllLogiciel'),
    path('logiciel/set/<int:pk>', views.setLogiciel, name='setLogiciel'),
    path('editeur', views.allEditeur, name='getAllEditeur'),
    path('editeur/set/<int:pk>', views.setEditeur, name='setEditeur'),
    path('utilisateur', views.allUtilisateur, name='getAllUtilisateur'),
    path('utilisateur/set/<int:pk>', views.setUtilisateur, name='setUtilisateur'),
    path('demande', views.allDemande, name='getAllDemande'),
    path('demande/set/<int:pk>', views.setDemande, name='setDemande'),
    path('demande/test', views.testDemande, name='testDemande'),
    path('test', views.testJS, name='testJS'),
    path('ajax/logiciel/<int:pk>', views.api_logiciel_id, name='api_logiciel_id'),
    path('ajax/dsi', views.all_dsi, name='all_dsi'),
    path('ajax/all', views.all_username, name='all_username'),
    path('ajax/metrique/<int:pk>', views.api_metrique_logiciel, name='api_metrique_logiciel'),
    path('ajax/demande', views.api_post_demande, name='api_post_demande'),
]