from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.core import serializers
import logging
import json


# Create your views here.



from django.views import generic

from .models import Demande, Logiciel, Editeur, Utilisateur, Dsi, Metrique
from .forms import UserForm, DemandeForm, ServeurForm, TarifForm, LogicielForm, EditeurForm, MetriqueForm, DsiForm


def new_utilisateur(request):
    if request.method == "POST":
        form = UserForm(request.POST)

        if form.is_valid():
            print("je suis valide")
            demande = form.save(commit=False)
            demande.save()

        else:
            print(form)
            print("je ne suis pas valide")
    else:
        form = UserForm()
    return render(request, 'licence/formUser.html', {'form': form})

def new_editeur(request):
    if request.method == "POST":
        form = EditeurForm(request.POST)

        if form.is_valid():
            editeur = form.save(commit=False)
            editeur.save()
    else:
        form = EditeurForm()
    return render(request, 'licence/formulaire.html', {'form': form})

def new_logiciel(request):
    if request.method == "POST":
        form = LogicielForm(request.POST)

        if form.is_valid():
            logiciel = form.save(commit=False)
            logiciel.save()
    else:
        form = LogicielForm()
    return render(request, 'licence/formulaire.html', {'form': form})

def new_tarif(request):
    if request.method == "POST":
        form = TarifForm(request.POST)

        if form.is_valid():
            tarif = form.save(commit=False)
            tarif.save()
    else:
        form = TarifForm()
    return render(request, 'licence/formulaire.html', {'form': form})

def new_serveur(request):
    if request.method == "POST":
        form = ServeurForm(request.POST)

        if form.is_valid():
            serveur = form.save(commit=False)
            serveur.save()
    else:
        form = ServeurForm()
    return render(request, 'licence/formulaire.html', {'form': form})

def new_demande(request):
    if request.method == "POST":
        form = DemandeForm(request.POST)

        if form.is_valid():
            demande = form.save(commit=False)
            demande.save()
    else:
        form = DemandeForm()
    return render(request, 'licence/formulaire.html', {'form': form})

def new_dsi(request):
    if request.method == "POST":
        form = DsiForm(request.POST)

        if form.is_valid():
            dsi = form.save(commit=False)
            dsi.save()
    else:
        form = DsiForm()
    return render(request, 'licence/formulaire.html', {'form': form})

def new_metrique(request):
    if request.method == "POST":
        form = MetriqueForm(request.POST)

        if form.is_valid():
            metrique = form.save(commit=False)
            metrique.save()
    else:
        form = MetriqueForm()
    return render(request, 'licence/formulaire.html', {'form': form})

def setLogiciel(request, pk):
    logiciel = get_object_or_404(Logiciel, id=pk)
    if request.method == "POST":
        form = LogicielForm(request.POST, instance=logiciel)
        if form.is_valid():
            logiciel = form.save(commit=False)
            logiciel.save()
            return redirect('/licence/logiciel')
    else:
        form = LogicielForm(instance=logiciel)
    return render(request, 'licence/formulaire.html', {'form': form, 'test': "test566"})

def setEditeur(request, pk):
    editeur = get_object_or_404(Editeur, id=pk)
    if request.method == "POST":
        form = EditeurForm(request.POST, instance=editeur)
        if form.is_valid():
            editeur = form.save(commit=False)
            editeur.save()
            return redirect('/licence/editeur')
    else:
        form = EditeurForm(instance=editeur)
    return render(request, 'licence/formulaire.html', {'form': form})

def setUtilisateur(request, pk):
    utilisateur = get_object_or_404(Utilisateur, id=pk)
    if request.method == "POST":
        form = UserForm(request.POST, instance=utilisateur)
        if form.is_valid():
            utilisateur = form.save(commit=False)
            utilisateur.save()
            return redirect('/licence/utilisateur')
    else:
        form = UserForm(instance=utilisateur)
    return render(request, 'licence/formulaire.html', {'form': form})

def setDemande(request, pk):
    demande = get_object_or_404(Demande, id=pk)
    if request.method == "POST":
        form = DemandeForm(request.POST, instance=demande)
        if form.is_valid():
            demande = form.save(commit=False)
            demande.save()
            return redirect('/licence/demande')
    else:
        form = DemandeForm(instance=demande)
    return render(request, 'licence/formulaire.html', {'form': form})



def allLogiciel(request):
    objects = Logiciel.objects.all()
    colonnes = ["Nom du logiciel", "Nom de l'editeur", "Descriptif", "Nombre de licences totales", "Nombre de licences restantes"]
    return render(request, 'licence/allLogiciel.html', {'objects': objects, 'colonnes': colonnes, })

def allEditeur(request):
    objects = Editeur.objects.all()
    colonnes = ["Nom de l'éditeur"]
    return render(request, 'licence/allEditeur.html', {'objects': objects, 'colonnes': colonnes, })

def allUtilisateur(request):
    objects = Utilisateur.objects.all()
    colonnes = ["CP", "Nom", "Prenom", "Adresse mail", "Entité", "Admin"]
    return render(request, 'licence/allUtilisateur.html', {'objects': objects, 'colonnes': colonnes, })

def allDemande(request):
    objects = Demande.objects.all()
    colonnes = ["Logiciel", "Statut", "Projet", "Quantité", "Demandeur"]
    return render(request, 'licence/allDemande.html', {'objects': objects, 'colonnes': colonnes, })

def index(request):
    return render(request, 'licence/index.html')

def testDemande(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        form2 = DemandeForm(request.POST)

        if form.is_valid():
            utilisateur = form.save(commit=False)
            utilisateur.save()
    else:
        form = UserForm()
        form2 = DemandeForm()
    return render(request, 'licence/allforms.html', {'form': form, 'form2': form2})

class DetailView(generic.DetailView):
    model = Demande
    template_name = 'licence/detail.html'

def validate_username(request):
    # request should be ajax and method should be GET.
    if request.is_ajax and request.method == "GET":
        # get the nick name from the client side.
        nick_name = request.GET.get("nick_name", None)
        # check for the nick name in the database.
        if Friend.objects.filter(nick_name=nick_name).exists():
            # if nick_name found return not valid new friend
            return JsonResponse({"valid": False}, status=200)
        else:
            # if nick_name not found, then user can create a new friend.
            return JsonResponse({"valid": True}, status=200)

    return JsonResponse({}, status=400)

def testJS(request):
    editeurs = Editeur.objects.all();
    form = UserForm();
    form3 = DemandeForm();
    return render(request, 'licence/testJS.html', {'form': form, 'form3':form3, 'editeurs': editeurs})

def all_username(request):
    # request should be ajax and method should be GET.

    if request.is_ajax and request.method == "GET":
        my_queryset = Utilisateur.objects.all()
        result_list = list(my_queryset.values('nom'))
        return HttpResponse(json.dumps(result_list))

def all_dsi(request):
    # request should be ajax and method should be GET.

    if request.is_ajax and request.method == "GET":
        my_queryset = Dsi.objects.all()
        result_list = list(my_queryset.values('nom'))
        return HttpResponse(json.dumps(result_list))

def api_logiciel_id(request, pk):
    # request should be ajax and method should be GET.
    if request.is_ajax and request.method == "GET":

        my_queryset = Logiciel.objects.filter(editeur=pk)
        result_list = list(my_queryset.values('nom', 'id'))
        return HttpResponse(json.dumps(result_list))

def api_all_editeur(request):
    # request should be ajax and method should be GET.

    if request.is_ajax and request.method == "GET":
        my_queryset = Editeur.objects.all()
        result_list = list(my_queryset.values('nom'))
        return HttpResponse(json.dumps(result_list))

def api_metrique_logiciel(request, pk):
    # request should be ajax and method should be GET.
    if request.is_ajax and request.method == "GET":

        my_queryset = Logiciel.objects.filter(id=pk)
        print(my_queryset)
        result_list = list(my_queryset.values('metrique__type'))
        return HttpResponse(json.dumps(result_list))

def api_post_demande(request):
    if request.method == 'POST':
        # no need to do this
        # request_csrf_token = request.POST.get('csrfmiddlewaretoken', '')
        request_getdata = request.POST.get('getdata', None)
        # make sure that you serialise "request_getdata"
        print(JsonResponse(request_getdata))
        print(JsonResponse(request_getdata))


