
        var catchError = function(e) {
            console.error('Erreur ajax', e)
        }
        var getTestMetrique = function () {

            return new Promise(function (resolve, reject) {
                get('http://127.0.0.1:8000/licence/ajax/metrique/1').then(function (response) {
                var metriques = JSON.parse(response)
                resolve(metriques)
            }).catch(catchError)
            })

        };
        getTestMetrique().then(function (metriques) {
            console.log(metriques[0].metrique__type)
        }).then(function () {
            console.log("Fin des appels ajax")
        })